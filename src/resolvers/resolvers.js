const db = require('../utils/db')
const shortid = require('shortid')

module.exports = {
  Query: {
    getShoppingCartByConsumer: async (_parent, args, _context, _info) => {
      const { getShoppingCartByConsumerInput } = args
      const shoppingCart = await db.collection('shoppingCart').findOne({ consumerId: getShoppingCartByConsumerInput.consumerId })
      return shoppingCart
    }
  },
  Mutation: {
    createShoppingCart: async (_parent, args, _context, _info) => {
      const { editShoppingCartInput } = args
      const existingShoppingCart = await db.collection('shoppingCart').findOne({ consumerId: editShoppingCartInput.consumerId })
      if (existingShoppingCart) {
        return existingShoppingCart
      } else {
        editShoppingCartInput._id = shortid.generate()
        editShoppingCartInput.products = []
        const createdShoppingCart = await db.collection('shoppingCart').insertOne(editShoppingCartInput)
        return createdShoppingCart.ops[0]
      }
    },
    addProduct: async (_parent, args, context, _info) => {
      const { editShoppingCartProductsInput } = args
      const consumerId = editShoppingCartProductsInput.consumerId
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == consumerId || user.accountType == 'ADMIN') {
        delete editShoppingCartProductsInput.consumerId
        const updatedShoppingCart = await db.collection('shoppingCart').findOneAndUpdate({ consumerId, 'products.productId': { $ne: editShoppingCartProductsInput.productId } }, { $push: { products: editShoppingCartProductsInput } }, { returnOriginal: false })
        return updatedShoppingCart.value
      } else {
        return null
      }

    },
    modifyProductAmount: async (_parent, args, context, _info) => {
      const { editShoppingCartProductAmount } = args
      const consumerId = editShoppingCartProductAmount.consumerId
      delete editShoppingCartProductAmount.consumerId
      var modifiedShoppingCart = null
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == consumerId || user.accountType == 'ADMIN') {
        if (editShoppingCartProductAmount.amount == 0) {
          modifiedShoppingCart = await db.collection('shoppingCart').findOneAndUpdate({ consumerId }, { $pull: { products: { productId: editShoppingCartProductAmount.productId } } }, { returnOriginal: false })
        } else {
          modifiedShoppingCart = await db.collection('shoppingCart').findOneAndUpdate({ consumerId, 'products.productId': editShoppingCartProductAmount.productId }, { $set: { 'products.$.amount': editShoppingCartProductAmount.amount } }, { returnOriginal: false })
        }
        return modifiedShoppingCart.value
      } else {
        return null
      }
    },
    emptyShoppingCart: async (_parent, args, context, _info) => {
      const { consumerId } = args.editShoppingCartInput
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == consumerId || user.accountType == 'ADMIN') {
        const emptiedShoppingCart = await db.collection('shoppingCart').findOneAndUpdate({ consumerId: consumerId }, { $set: { products: [] } }, { returnOriginal: false })
        return emptiedShoppingCart.value
      } else {
        return null
      }

    },
    removeShoppingCart: async (_parent, args, _context, _info) => {
      const { editShoppingCartInput } = args
      await db.collection('shoppingCart').deleteOne({ consumerId: editShoppingCartInput.consumerId })
      return null
    }
  }
}
