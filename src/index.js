require('dotenv').config()

const { ApolloServer, gql, PubSub } = require('apollo-server')
const { buildFederatedSchema } = require('@apollo/federation')
const fs = require('fs')
const path = require('path')
const config = require('../config')
const resolvers = require('./resolvers/resolvers')
const db = require('./utils/db')

const url = `mongodb://${process.env.MONGO_USER || 'root'}:${process.env.MONGO_PASSWORD || 'example'}@${process.env.MONGO_SERVER || 'localhost'}:27017`

const types = fs.readFileSync(path.join(__dirname, './schemas/schemas.graphql'), 'utf8')

const pubsub = new PubSub()

const schema = buildFederatedSchema([{ typeDefs: gql`${types}`, resolvers }])

const server = new ApolloServer({
  schema,
  context: req => ({
    ...req,
    pubsub,
  }),
  playground: {
    endpoint: config.app.playground,
  },
  subscriptions: config.app.subscriptions,
})

db.connect(url, err => {
  if (err) {
    console.log('Unable to connect to database.')
  } else {
    server
      .listen({ port: (process.env.SERVICE_PORT || config.app.port), endpoint: config.app.endpoint })
      .then(({ url, subscriptionsPath }) => {
        console.log(url)
        console.log(subscriptionsPath)
        console.log(`Server is running on ${url}`)
      })
  }
})

process.on('SIGINT', () => {
  process.exit()
})